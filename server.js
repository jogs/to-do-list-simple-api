'use strict';
(function() {
	var port = process.env.PORT || 19170;
  var express = require('express');
  var app = express();
	var bodyParser = require('body-parser');

	app.use(bodyParser.json());

  // code

	app.use(function(req,res){
		res.type('text')
			.status(404)
			.send('404 - No Encontrado');
	})
	.use(function(err,req,res,next) {
		console.error(err.stack);
		res.type('text')
			.status(500)
			.send('500 - error en el servidor')
	})
	.listen(port,function(){
		console.log('Express corriendo en el puerto '+port+';\n- Prisione Ctrl + C para detener el servicio\n- En un entorno de '+app.get('env'));
	});

  module.exports = app;
})();
